<?php
namespace application\controller;
use \application\library as Library;

class main extends \simpleton\framework\controller
{
	public function index()
	{
		$lib = new Library\sample;
		
		$libHash = new \construct7\auth\library\hash;
		$libHash->login();
		
		//$this->router->route( 'testapp/main/index' );
		//echo '<p>'.__CLASS__.'</p>';
		
		include $this->view('index' );
	}
	
	public function error( $option = array() )
	{
		$exception = NULL;
		$error     = NULL; // When fatal
		extract( $option, EXTR_IF_EXISTS );
		
		$trace = $this->router->trace;
		include $this->view('error');
	}
}
