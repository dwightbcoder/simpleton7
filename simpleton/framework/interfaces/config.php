<?php
namespace simpleton\framework\interfaces;

interface config
{
	// Get or set an option
	public function option( string $option, $value = FALSE );
	// Get or set an option default value
	public function default( string $option, $value = FALSE );
	// Reset one option or all options to default
	public function reset( $option = FALSE ) : config;
	// Check if a config has an option set
	public function has( string $option ) : bool;
	// Listen to config option value changes
	public function listen( string $option, callable $callback, bool $on = true ) : config;
}
