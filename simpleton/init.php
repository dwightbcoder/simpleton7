<?php
include 'framework/core.php';

$config = new \simpleton\framework\config();
$config
	//->option( 'default_application', 'construct7_auth' )
	->option( 'trace', 1 )
	->option( 'theme', 'default' );

$simpleton = new \simpleton\framework\core( $config );

// We should auto-detect this -- should apps be stored in simpleton/app? We could have simpleton/app/default
// what about per-domain app settings? (should be able to handle this with config default_application)
//$simpleton->router->app( 'testapp', 'test/' );
$simpleton->package( 'construct7_auth', 'auth' );


//$simpleton->router->alias('short/','testapp/main/' );
//$simpleton->router->alias('fun/', 'main/' );

$simpleton->bootstrap();
