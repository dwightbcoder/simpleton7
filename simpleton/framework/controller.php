<?php
namespace simpleton\framework;
include 'interfaces/controller.php';

class controller
{
	public $simpleton;
	private $_path = '';
	protected $router = null;
	
	function __construct( interfaces\core $simpleton )
	{
		$this->simpleton = $simpleton;
		$this->__setup();
	}
	
	protected function __setup()
	{
		// stub
	}
	
	protected function path()
	{
		if ( ! $this->_path )
		{
			$this->_path = dirname((new \ReflectionClass($this))->getFileName()) . '/..';
		}
		
		return $this->_path;
	}
	
	protected function view( string $view, string $base = '' )
	{		
		$path = '';
		$view_file = $base . $view . '.phtml';
		$theme_path = $this->simpleton->config->option('theme');
		
		if ( $theme_path )
		{
			$theme_path = $this->simpleton->config->option('dir_simpleton') . DIRECTORY_SEPARATOR . 'theme' . DIRECTORY_SEPARATOR . $theme_path . DIRECTORY_SEPARATOR;
			$theme_path .= ($base && is_string($base) ? $base : str_replace(array('\\controller\\','\\'),DIRECTORY_SEPARATOR,get_class($this)) . DIRECTORY_SEPARATOR);
			
			if ( file_exists($theme_path . $view_file) )
			{
				$path = $theme_path;
			}
		}
		
		if ( ! $path )
		{
			$base = $base && is_string($base) ? $base : basename(str_replace('\\',DIRECTORY_SEPARATOR,get_class($this))) . DIRECTORY_SEPARATOR;
			$path = $this->path() . '/view/' . $base;
		}
		
		return $path . $view_file;
	}
}
