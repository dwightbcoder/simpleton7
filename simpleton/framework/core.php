<?php
namespace simpleton\framework;
include 'interfaces/core.php';
include 'config.php';
include 'event.php';
include 'controller.php';

class core implements interfaces\core
{
	public $trace = [];
	
	private $config   = null; // Config class
	private $event    = null; // Event class
	private $_package = []; // Registered apps/dependencies
	private $_alias   = ['search'=>[], 'replace'=>[]];
	
	function __construct( interfaces\config &$config = null, interfaces\event &$event = null )
	{
		$this->config = $config ? $config : new config;
		$this->config
			->listen( 'autoload'        , [__CLASS__, '_autoload_toggle'] )
			->listen( 'handle_fatal'    , [$this,'_fatal_toggle'] )
			->listen( 'handle_exception', [$this,'_exception_toggle'] )
			->listen( 'handle_error'    , [$this,'_error_toggle'] )
			->default( 'dir_simpleton', realpath(__DIR__.DIRECTORY_SEPARATOR.'..') )
			->default( 'trace'              , 0 )
			->default( 'autoload'           , 1 )
			->default( 'handle_fatal'       , 1 )
			->default( 'handle_exception'   , 1 )
			->default( 'handle_error'       , 1 )
			->default( 'error_controller'   , 'main' )
			->default( 'error_method'       , 'error' )
			->default( 'default_application', 'application' )
			->default( 'default_controller' , 'main' )
			->default( 'default_method'     , 'index' );
		
		$this->event  = $event ? $event : new event;
	}
	
	// ! Routing
	
	public function bootstrap()
	{
		$route = isset($_REQUEST['__SIMPLETON__']) ? $_REQUEST['__SIMPLETON__'] : '';
		$this->route( $route );
	}
	
	public function route( string $route, array $data = [] )
	{
		$class = null;
		$instance = null;
		
		if ( $this->config->option('trace') )
		{
			$this->trace[] = [
				'microtime' => microtime(1),
				'route'     => $route,
				'data'      => $data
			];
		}
		
		list( $application, $controller, $method, $data ) = $this->parse( $route, $data );
		
		try
		{
			$class = $application . '\\controller\\' . $controller;
			try
			{
				$instance = new $class( $this );
			}
			catch( \Exception $e )
			{
				throw new Exception( 'Controller not found: ' . $class, ERR_MISSING_CONTROLLER, $e );
			}
			
			if ( $instance && method_exists($instance, $method) )
			{
				$instance->$method( $data );
			}
			elseif ( $controller == $this->config->option('error_controller') && $method == $this->config->option('error_method') )
			{
				throw new Exception( 'Error handler not found: ' . $controller . '/' . $method, ERR_NO_ERROR_HANDLER );
			}
			else
			{
				throw new Exception( 'Method not found: ' . $class.'::'.$method, ERR_MISSING_METHOD );
			}

		}
		catch( Exception $e )
		{
			if ( $e->getCode() != ERR_NO_ERROR_HANDLER )
			{
				$this->event->trigger( 'System.404', array($controller, $method, $data) );
				throw new Exception( 'Page not found: ' . $route, ERR_BAD_ROUTE, $e );
			}
			else
			{
				throw $e;
			}
		}
	}
	
	public function alias( string $search, string $replace )
	{
		$this->_alias['search'][]  = $search;
		$this->_alias['replace'][] = $replace;
		return $this;
	}
	
	public function url( string $controller = null, string $method = null, array $data = [] )
	{
		$controller = str_replace('\\', '-', $controller);
		return $controller . '/' . $method . '?' . http_build_query( $data );
	}
	
	public function package( string $package, string $alias = '' )
	{
		$this->_package[] = $package;
		if ( $alias )
		{
			$this->alias( "/$alias/", "/$package/" );
		}
	}
	
	
	/**
	 * Parse route string
	 * @param string Route string to parse
	 * @return array controller (string), method (string), data (array)
	 */
	public function parse( string $route, array $data = [] )
	{
		$route      = $route ? $route : $this->config->option('default_controller');
		if ( substr($route, 0, 1) != '/' )
		{
			$route = '/' . $route;
		}
		if ( substr($route, -1, 1) != '/' )
		{
			$route .= '/';
		}
		$route      = str_replace($this->_alias['search'], $this->_alias['replace'], $route);
		$parts      = explode( '/', $route );
		$app        = $this->config->option('default_application');
		array_shift( $parts );
		
		if ( in_array($parts[0], $this->_package) )
		{
			$app = $parts[0];
			array_shift( $parts );
		}
		
		$controller = $parts[0] ? $parts[0] : $this->config->option('default_controller');
		$method     = '';
		
		if ( isset($parts[1]) )
		{
			$method = $parts[1];
			
			if ( isset($parts[2]) )
			{
				$_data = array_slice( $parts, 2 );
				
				for ( $i = 0; $i < count($_data); $i += 2 )
				{
					if ( $_data[$i] )
					{
						$value = NULL;
						if ( isset($_data[$i + 1]) )
						{
							$value = $_data[$i + 1];
						}
						
						$data[$_data[$i]] = $value;
					}
				}
			}
		}
		
		if ( ! $method )
		{
			$method = $this->config->option('default_method');
		}
		
		$app = str_replace(array('_','-'), '\\', $app);
		$controller = str_replace(array('_','-'), '\\', $controller);
		return [$app, $controller, $method, $data];
	}
	
	
	// ! Error handling
	
	public function error( $errno, $errstr, $errfile, $errline )
	{
	    if ( $this->config->option('handle_fatal') || ! (error_reporting() & $errno) )
	    {
	        return FALSE;
	    }
		
		throw new \Exception( $errstr, $errno );
		
		return TRUE;
	}
	
	public function exception( $e, $fatal = FALSE )
	{
		$this->event->trigger( 'System.Exception', array($e, $fatal) );
		
		if ( ! $fatal || ($fatal && $this->config->option('handle_fatal')) )
		{
			$route = $this->config->option('error_controller') . '/' . $this->config->option('error_method');
			$this->route( $route, array('exception' => $e) );
		}
	}
	
	public function shutdown()
	{
		if ( ! $this->config->option('handle_fatal') )
		{
			return;
		}
		
		$error = error_get_last();
		if ( $error )
		{
			$e = new \Exception(
				$error['message'],
				$error['type']
			);
	
			$this->exception( $e, TRUE );
		}
	}
	
	public function _fatal_toggle( config $config )
	{
		if ( $config->option('handle_fatal') )
		{
			register_shutdown_function( [$this, 'shutdown'] );
		}
	}
	
	public function _exception_toggle( config $config )
	{
		if ( $config->option('handle_exception') )
		{
			set_exception_handler( [$this, 'exception'] );
		}
		else
		{
			restore_exception_handler();
		}
	}
	
	public function _error_toggle( config $config )
	{
		if ( $config->option('handle_error') )
		{
			set_error_handler( [$this, 'error'] );
		}
		else
		{
			restore_error_handler();
		}
	}

	
	// ! Config
	
	public function __get( string $name )
	{
		if ( $name == 'config' )
		{
			return $this->config;
		}
		elseif ( $name == 'event' )
		{
			return $this->event;
		}
	}
	
	
	// ! Autoloading
	
	public static function autoload( string $class )
	{

		$class = str_replace('_','\\', $class);
		$file = str_replace('\\',DIRECTORY_SEPARATOR, $class);
		$include = '../simpleton/' . $file . '.php';
		if ( file_exists($include) )
		{
			include $include;
			return TRUE;
		}
		
		$include = '../simpleton/package/' . $file . '.php';		
		if ( file_exists($include) )
		{
			include $include;
			return TRUE;
		}
		
		return FALSE;
	}
	
	public static function _autoload_toggle( interfaces\config $config )
	{
		if ( $config->option('autoload') )
		{
			spl_autoload_register( [__CLASS__, 'autoload'] );
		}
		else
		{
			spl_autoload_unregister( [__CLASS__, 'autoload'] );
		}
	}
	

}

class Exception extends \Exception {}
