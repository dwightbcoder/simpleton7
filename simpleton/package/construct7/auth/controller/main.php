<?php
namespace construct7\auth\controller;
use construct7\auth\library as Library;

class main extends \simpleton\framework\controller
{
	public $auth = null;
	
	protected function __setup()
	{
		$this->auth = new Library\hash;
	}
	
	public function index( $option = [] )
	{
		$this->auth->login();
		include $this->view('index' );
	}
}
