<?php
namespace simpleton\framework\interfaces;

interface controller
{
	public function __construct( core $simpleton );
	public function path();
	public function view( string $view, string $base = '' );
}
