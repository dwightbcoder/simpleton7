<?php
namespace simpleton\framework\interfaces;

interface event
{
	/**
	 * Add an event callback
	 */
	public function on( string $event, callable $callback );
	
	/**
	 * Remove an event callback
	 */	
	public function off( string $event, callable $callback );
	
	/**
	 * Trigger all registered event callbacks
	 */
	public function trigger( string $event, array $arguments = array() );
}
