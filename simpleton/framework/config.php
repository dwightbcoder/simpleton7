<?php
namespace simpleton\framework;
include 'interfaces/config.php';

class config implements interfaces\config
{
	private $_option  = array();
	private $_default = array();
	private $_listen  = array();
	
	public function option( string $option, $value = FALSE )
	{
		if ( $value === FALSE )
		{
			if ( isset($this->_option[$option]) )
			{
				return $this->_option[$option];
			}
			
			return FALSE;
		}
		
		if ( ! isset($this->_option[$option]) || $this->_option[$option] != $value )
		{
			$old = isset($this->_option[$option]) ? $this->_option[$option] : FALSE;
			
			$this->_option[$option] = $value;
			
			if ( isset($this->_listen[$option]) )
			{
				$this->_trigger( $option, $old );
			}
		}

		return $this;
	}
	
	public function default( string $option, $value = FALSE )
	{
		if ( $value === FALSE )
		{
			return isset($this->_default[$option]) ? $this->_default[$option] : FALSE;
		}
		
		$this->_default[$option] = $value;
		
		if ( ! $this->has($option) )
		{
			$this->option( $option, $value );
		}
		
		return $this;
	}
	
	public function reset( $option = FALSE ) : interfaces\config
	{
		if ( $option === FALSE )
		{
			return $this->set( $option, $this->default($option) );
		}
		
		$key = array_keys( $this->_option );
		foreach ( $key as $_key )
		{
			$this->reset( $_key );
		}
		
		return $this;
	}
	
	public function has( string $option ) : bool
	{
		return array_key_exists( $option, $this->_option );
	}
	
	public function listen( string $option, callable $callback, bool $on = true ) : interfaces\config
	{
		if ( ! isset($this->_listen[$option]) )
		{
			$this->_listen[$option] = array();
		}
		
		// Callback already exists
		$existing = array_search( $callback, $this->_listen[$option] );
		
		if ( $on && ($existing === FALSE || $existing === NULL) )
		{
			// Add listener
			$this->_listen[$option][] = $callback;
		}
		elseif ( ! $on )
		{
			// Remove listener
			if ( $existing !== FALSE && $existing !== NULL )
			{
				unset( $this->_listen[$option][$existing] );
			}
		}
		
		return $this;
	}
	
	private function _trigger( string $option, $old = FALSE )
	{
		if ( isset($this->_listen[$option]) )
		{
			foreach ( $this->_listen[$option] as $_callback )
			{
				call_user_func_array( $_callback, array(&$this, $option, $this->_option[$option], $old) );
			}
		}
	}
}
