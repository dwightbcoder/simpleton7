<?php
namespace simpleton\framework;
include 'interfaces/event.php';
const ERR_BAD_CALLBACK = 501;

class event implements interfaces\event
{
	private $_event = array();
	
	/**
	 * Add an event callback
	 */
	public function on( string $event, callable $callback )
	{
		if ( is_callable($callback) )
		{
			if ( ! isset($this->_event[$event]) )
			{
				$this->_event[$event] = array();
			}
			
			$this->_event[$event][] = $callback;
			
			return TRUE;
		}
		
		throw new Exception( 'Callback is not callable: ' . (string)$callback, ERR_BAD_CALLBACK );	
		
		return FALSE;
	}
	
	/**
	 * Remove an event callback
	 */	
	public function off( string $event, callable $callback )
	{
		if ( $callback === FALSE )
		{
			unset( $this->_event[$event] );
			return TRUE;
		}
		else
		{
			foreach( $this->_event[$event] as $i => $_callback )
			{
				if ( $_callback === $callback )
				{
					unset( $this->_event[$event][$i] );
					return TRUE;
				}
			}
		}
		
		return FALSE;
	}
	
	/**
	 * Trigger all registered event callbacks
	 */
	public function trigger( string $event, array $arguments = array() )
	{
		if ( isset($this->_event[$event]) )
		{
	        foreach( $this->_event[$event] as $_callback )
	        {
				$return = call_user_func_array( $_callback, array(&$arguments) );
				if ( $return === FALSE )
				{
					break;
				}
	        }
	        
	        return $arguments;
		}
		
		return $arguments;
	}

}
